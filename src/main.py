import module1 as m1  # Импорт модуля с функциями из части 1
import module2 as m2  # Импорт модуля с функцией из части 2

text = "\nВведите <1>, если хотите создать массив случайных числел. " \
       "Введите <2>, если хотите отсортировать студентов по оценкам. " \
       "Введите <0> для выхода.\n"

module_select = input(text)
while module_select != '0':
    if module_select == '1':
        size = int(input("\nВведите размер массива: "))  # Получение размерности массива от пользователя
        array = m1.generate_array(size)  # Генерация массива
        m1.print_array(array)  # Вывод массива
        module_select = input(text)
    elif module_select == '2':
        file_path = '../data/Exams.csv'  # Путь к файлу
        m2.sort_students_by_subject(file_path)  # Вывод таблицы
        module_select = input(text)
    else:
        print("Неверный ввод!")
        module_select = input(text)
