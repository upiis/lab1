import pandas as pd  # Импорт библиотеки для работы с таблицами


def sort_students_by_subject(file_path):
    df = pd.read_csv(file_path, encoding='cp1251')  # Считывание данных из файла
    unique_subjects = df['Предмет'].unique()  # Вывод всех уникальных значений столбца "Предмет"
    print('Выберите предмет для сортировки:', unique_subjects)

    subject = None
    while subject not in unique_subjects:  # Ввод предмета пользователем и проверка, что он существует
        subject = input()
        if subject not in unique_subjects:
            print('Некорректный ввод, попробуйте еще раз.')

    filtered_df = df[df['Предмет'] == subject]  # Сортировка таблицы по выбранному предмету
    sorted_df = filtered_df.sort_values(by=['Оценка'], ascending=False)

    table = sorted_df[['ФИО', 'Дата рождения']]  # Вывод таблицы с ФИО и датой рождения студента
    print(table.to_string(index=False))