# Импорт библиотеки для работы с массивами
import numpy as np


# Функция создания массива на основе введенного пользователем размера
def generate_array(size):
    return np.random.rand(size)


# Функция вывода массива на экран
def print_array(array):
    print(array)
